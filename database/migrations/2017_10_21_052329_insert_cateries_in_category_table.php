<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertCateriesInCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('category')->insert(
            array(
                array(
                    'name' => 'pinturas',
                    'url' => '/galeria/pinturas'
                ),
                array(
                'name' => 'esculturas',
                'url' => '/galeria/esculturas'
            ),
                array(
                    'name' => 'esculto_pinturas',
                    'url' => '/galeria/esculto_pinturas'
                ),
                array(
                    'name' => 'vitromosaiquismo',
                    'url' => '/galeria/vitromosaiquismo'
                )
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('category')->truncate();
    }
}

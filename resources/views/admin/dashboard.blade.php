@extends('layouts.admin.master')

@section('content')
<div id="page-wrapper">
	<div class="main-page">
		<div class="row-one">
			<div class="col-md-3 widget">
				<div class="stats-left ">
					<h5>Hoy</h5>
					<h4>Comentarios</h4>
				</div>
				<div class="stats-right">
					<label> {{ $commentsCount }}</label>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="col-md-3 widget states-mdl">
				<div class="stats-left">
					<h5>Hoy</h5>
					<h4>Visitas</h4>
				</div>
				<div class="stats-right">
					<label> {!! $viewsToday !!}</label>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="col-md-3 widget states-last">
				<div class="stats-left">
					<h5>Total</h5>
					<h4>Visitas</h4>
				</div>
				<div class="stats-right">
					<label>{!! $viewsTotal !!}</label>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>

		<div class="clearfix"> </div>

		<div class="row">
			<div class="col-md-4 stats-info widget">
				<div class="stats-title">
					<h4 class="title">5 Obras mas populares</h4>
				</div>
				<div class="stats-body">
					<ul class="list-unstyled">
						@if(!$bestWorks->count()) <li><h4>No hay obras comentadas</h4></li> @endif
						@foreach($bestWorks as $work)
							<li>
								<img src="{{ url($work->image_url) }}" alt="" style="height: 64px; width: 64px;"> {{ $work->title }} <span class="pull-right">{{ $work->comments->count() }} comentarios</span>
							</li>
						@endforeach
					</ul>
				</div>
			</div>
			<div class="col-md-8 stats-info stats-last widget-shadow">
				<div class="stats-title">
					<h4 class="title">Ultimos comentarios</h4>
				</div>
				<table class="table table-hover stats-table ">
					<thead>
					<tr>
						<th>Fecha del comentario</th>
						<th>Nombre</th>
						<th>Comentario</th>
					</tr>
					</thead>
					<tbody>
					@if(!$last5Comments->count()) <tr><td>No hay comentarios</td></tr> @endif
					@foreach($last5Comments as $comment)
						<tr onclick="window.document.location='{{ route('single.work', $comment->gallery_id) }}#comment-section';">
							<td>{{ $comment->created_at }}</td>
							<td>{{ str_limit($comment->guest_name, 20) }}</td>
							<td>{{ str_limit($comment->comment, 30) }}</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection
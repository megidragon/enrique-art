@extends('layouts.admin.master')
@section('content')
<div class="main-page login-page ">
    <h3 class="title1">Ingreso al panel de administracion</h3>
    <div class="widget-shadow">
        <div class="login-top">
            <h4>Bienvenido al panel de administracion de <h2>Enrique Art</h2></h4>
        </div>
        <div class="login-body">
            <form action="{{route('login.post')}}" method="post">
                <input type="text" class="user" name="email" placeholder="Ingrese su usuario" required="">
                <input type="password" name="password" class="lock" placeholder="Contraseña">
                {{csrf_field()}}
                <div class="forgot-grid">
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"> </div>
                </div>
                <input type="submit" value="Iniciar sesion">
            </form>
        </div>
    </div>
</div>
@endsection
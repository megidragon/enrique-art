@extends('layouts.admin.master')

@section('content')
<div id="page-wrapper">
    <div class="main-page">
        <div class="media">
            <h3 class="title1">Listado de todos los comentarios</h3>
            <div class="bs-example5 widget-shadow" data-example-id="default-media">
                @if(!$commentaries->count())
                    <h4>No hay comentarios</h4>
                @endif
                @foreach($commentaries as $commentary)
                    <div class="media">
                        <div class="media-left">
                            @if($commentary->gallery)
                                <a href="{{ route('single.work', $commentary->gallery->id) }}">
                                    <img class="media-object" alt="64x64" src="{!!$commentary->gallery->image_url !!}" data-holder-rendered="true" style="width: 120px; height: 120px;">
                                </a>
                            @else
                                <img class="media-object" alt="64x64" src="{{ url('images/perfil.jpg') }}" data-holder-rendered="true" style="width: 120px; height: 120px;">
                            @endif
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">
                                Nombre: {!! ucfirst($commentary->guest_name) !!}
                            </h4>
                            Comentado el dia: {!! ucfirst($commentary->created_at) !!}
                            <br>
                            Comentario: {!! ucfirst($commentary->comment) !!}
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                @endforeach
                    @include('layouts.paginate', ['results' => $commentaries])
            </div>
        </div>
    </div>
</div>
@stop
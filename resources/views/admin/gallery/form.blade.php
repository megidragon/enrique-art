@extends('layouts.admin.master')

@section('content')
<div id="page-wrapper">
    <div class="main-page">
        <div class="forms">
            <div class="row">
                <h3 class="title1">Cargar nueva obra: </h3>
                <div class="form-three widget-shadow">
                    <form class="form-horizontal" id="galleryForm" action="{{route('admin.gallery.save')}}" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="focusedinput" class="col-sm-2 control-label">Titulo</label>
                            <div class="col-sm-8">
                                <input type="text" name="title" class="form-control1" id="focusedinput" placeholder="Titulo" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="focusedinput" class="col-sm-2 control-label">Medidas</label>
                            <div class="col-sm-8">
                                <input type="text" name="measurements" class="form-control1" id="focusedinput" required="required" placeholder="23cm x 23cm x 23cm">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="focusedinput" class="col-sm-2 control-label">Tecnica</label>
                            <div class="col-sm-8">
                                <input type="text" name="technique" class="form-control1" id="focusedinput" placeholder="Mascara">
                            </div>
                            <div class="col-sm-2">
                                <p class="help-block">(Opcional)</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Fecha de creacion</label>
                            <div class="col-md-2">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar "></i>
                                    </span>
                                    <input type="date" name="creation_date" class="form-control1" required="required" placeholder="Fecha">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="radio" class="col-sm-2 control-label">Categoria</label>
                            <div class="col-sm-8">
                                @if(isset($categories))
                                     @foreach($categories as $category)
                                        <div class="radio-inline">
                                            <label><input type="radio" value="{{$category->id}}" name="category_id"> {{ucfirst($category->name)}}</label>
                                        </div>
                                     @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="textarea-gallery" class="col-sm-2 control-label">Descripcion</label>
                            <div class="col-sm-8">
                                <textarea name="description" id="textarea-gallery" cols="50" rows="4" class="form-control1"></textarea>
                            </div>
                            <div class="col-sm-2">
                                <p class="help-block">(Opcional)</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="exampleInputFile">Cargar fotos</label>
                            <input type="file" name="image" id="exampleInputFile" required="required">
                            <div class="col-md-offset-2">
                                <p class="help-block">Elija imagenes de su obra guardadas en su computadora</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-2">
                                <div class="checkbox-inline1">
                                    <label><input type="checkbox" name="continue" value="1" checked="checked"> ¿Publicar y seguir creando?</label>
                                </div>
                            </div>
                        </div>
                        {{csrf_field()}}
                        <div class="col-md-offset-2">
                            <button type="submit" class="btn btn-default">Publicar obra</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('additional')
    <link rel="stylesheet" href="{{url('/css/admin/gallery/form.css')}}">
    <script>

    </script>
@endsection
@extends('layouts.admin.master')

@section('content')
<div id="page-wrapper">
	<div class="main-page">
		<div class="media">
			<h3 class="title1">Listado de obras
                <a href="{{route('admin.gallery.form')}}">
                    <button class="btn btn-primary btn-lg header-right">Añadir nueva obra</button>
                </a>
            </h3>
			<div class="bs-example5 widget-shadow" data-example-id="default-media">
				@if(!$galleries->count())
					<h4>Sin obras cargadas</h4>
				@endif
				@foreach($galleries as $gallery)
					<div class="media">
						<div class="media-left">
							<a href="{{ route('single.work', $gallery->id) }}">
							  <img class="media-object" alt="64x64" src="{!! $gallery->image_url !!}" data-holder-rendered="true" style="width: 120px; height: 120px;">
							</a>
						</div>
						<div class="media-body">
							<h4 class="media-heading">{!! ucfirst($gallery->title) !!}</h4>
							@if(isset($gallery->category))
								Categoria: {!! ucfirst($gallery->category->name) !!}
								<br>
							@endif
							Medidas: {!! ucfirst($gallery->measurements) !!}
							<br>
							Descripcion: {!! ucfirst($gallery->description) !!}
							<br>
							<a href="{{ route('admin.gallery.edit', $gallery->id) }}">
								<i class="fa fa-pencil  fa-lg"></i>
								Modificar
							</a>
						</div>
						<div class="media-right">
							@if($gallery->comments->count())
								<a href="{{ route('admin.commentary', $gallery->id) }}">
									<span style="background-color: lightgreen; margin-left: 10px;" class="badge"> {{ $gallery->comments->count() }} </span>
									<i class="fa fa-comment fa-lg"></i>
								</a>
							@else
								<i class="fa fa-comment fa-lg"></i>
							@endif
						</div>
						<div class="clearfix"> </div>
					</div>
				@endforeach
					@include('layouts.paginate', ['results' => $galleries])
			</div>
		</div>
	</div>
</div>
@endsection
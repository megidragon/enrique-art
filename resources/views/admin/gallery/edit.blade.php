@extends('layouts.admin.master')

@section('content')
<div id="page-wrapper">
    <div class="main-page">
        <div class="forms">
            <div class="row">
                <h3 class="title1">Cargar nueva obra: </h3>
                <div class="form-three widget-shadow">
                    <form class="form-horizontal" id="galleryForm" action="{{route('admin.gallery.edit', $work->id)}}" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="focusedinput" class="col-sm-2 control-label">Titulo</label>
                            <div class="col-sm-8">
                                <input type="text" name="title" class="form-control1" id="focusedinput" placeholder="Titulo" value="{{ $work->title }}" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="focusedinput" class="col-sm-2 control-label">Medidas</label>
                            <div class="col-sm-8">
                                <input type="text" name="measurements" class="form-control1" id="focusedinput" value="{{ $work->measurements }}" required="required" placeholder="23cm x 23cm x 23cm">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="focusedinput" class="col-sm-2 control-label">Tecnica</label>
                            <div class="col-sm-8">
                                <input type="text" name="technique" class="form-control1" id="focusedinput" value="{{ $work->technique }}" placeholder="Mascara">
                            </div>
                            <div class="col-sm-2">
                                <p class="help-block">(Opcional)</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="radio" class="col-sm-2 control-label">Categoria</label>
                            <div class="col-sm-8">
                                @if(isset($categories))
                                     @foreach($categories as $category)
                                        <div class="radio-inline">
                                            <label><input type="radio" value="{{$category->id}}" @if($category->id == $work->category_id) checked @endif name="category_id"> {{ucfirst($category->name)}}</label>
                                        </div>
                                     @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="textarea-gallery" class="col-sm-2 control-label">Descripcion</label>
                            <div class="col-sm-8">
                                <textarea name="description" id="textarea-gallery" cols="50" rows="4" class="form-control1">{{ $work->description }}</textarea>
                            </div>
                            <div class="col-sm-2">
                                <p class="help-block">(Opcional)</p>
                            </div>
                        </div>
                        {{csrf_field()}}
                        <div class="col-md-offset-2">
                            <button type="submit" class="btn btn-default">Guardar cambios</button>
                        </div>

                    </form>
                    <div class="row">
                        <form action="{{ route('admin.gallery.delete', $work->id)  }}" method="post">
                            {{csrf_field()}}
                            <div class="pull-right">
                                <button onclick="if(confirm('¿Seguro desea eliminar esta obra?')){ return true; }else{ return false; }" class="btn btn-danger">Eliminar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('additional')
    <link rel="stylesheet" href="{{url('/css/admin/gallery/form.css')}}">
    <script>

    </script>
@endsection
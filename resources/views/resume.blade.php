@extends('layouts.master')

@section('content')
<!-- Main -->
<div class="wrapper style1">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <img src="{{ url('images/enrique.jpg') }}" alt="">
            </div>
            <div class="col-md-9">
                <h1>Enrique Hofman</h1>
                <br>
                <p>
                    Soy un artista plastico con más de 60 años de experiencia en el arte.
                </p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <h3>Experiencia:</h3>
            </div>
            <div class="col-md-12">
                <ul>
                    <li>En el año 1955 Frecuenta el taller del artista Demetrio Urruchua</li>
                    <li><br></li>
                    <li>En el año 1957 estudio con el maestro Juan Batle Pianas</li>
                    <li><br></li>
                    <li>1975 Frecuento el taller de la Pintora Mara Sanchez.</li>
                    <li><br></li>
                    <li>1980 Frecuenta el taller del Pintor Miguel Davila.</li>
                    <li><br></li>
                    <li>1985 toma clases con el Pintor Bruno Venier</li>
                    <li><br></li>
                    <li>2005 toma clases de Vitrofusión con el Artista Carlos Herzberg.</li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <h3>
                    Muestras y Exposiciones:
                </h3>
            </div>
            <div class="col-md-12">
                <ul>
                    <li>1957 - Salón Alumnos de la Facultad de Ciencias Económicas. (Mencion Honorifica)</li>
                    <li><br></li>
                    <li>Mayo 2007 - Exposición permanente de un Vitro Menorah en Templo Emanuel de Belgrano</li>
                    <li><br></li>
                    <li>Octubre 2008 - Exposición Colectiva de la 7ma. Muestra de Pintura y Escultura Liga Country Sur</li>
                    <li><br></li>
                    <li>Diciembre 2008 - Muestra de Alumnos de Carlos Herzerg Galeria Bestiario</li>
                    <li><br></li>
                    <li>Febrero 2009 - Exposición Colectiva Muestra Libre Expresión 2009 Galeria Barque (Mencion Honorifica)</li>
                    <li><br></li>
                    <li>Marzo 2009 - Galeria Barque. Salon Otoño 2009, 2 trabajos seleccionados</li>
                    <li><br></li>
                    <li>Abril 2009 - Seleccionado Muestra SAlón de Arte Abstracto 2009. Galeria Barque.</li>
                    <li><br></li>
                    <li>8 al 21 Mayo 2009 - Muestra individual. Galeria Barque</li>
                    <li><br></li>
                    <li>28 de mayo 2009 - Seleccionado Salón Pintura y Nuevas Exoeriencias 2009 Galeria Barque.</li>
                    <li><br></li>
                    <li>10 de Junio - Seleccionado para la muestra colectiva Camino al Bicentenario desde el 19 de Junio al 1 de Julio de 2009.</li>
                    <li><br></li>
                    <li>31 de Octubre al 14 de Noviembre 2009 - Exposicion Colectiva VIII Encuentro de Pintura y Escultuira Country Sur.</li>
                    <li><br></li>
                    <li>9 al 13 de Noviembre 2009 - Exposición Colectiva 14° Edición Unioversidad de Belgrano "Mas alla de los 70"</li>
                    <li><br></li>
                    <li>10 al 13 de Mayo de 2010 - Exposicion Colectiva Eniarte Palacio San Miguel.</li>
                    <li><br></li>
                    <li>15 de Octubre al 4 de Septiembre 2010 - Muestra individual en el Espacio de Cultura de Clib de Campo "La Martona"</li>
                    <li><br></li>
                    <li>4 al 11 de Noviembre 2010 - 15° Muestra "Mas alla de los 70" Invitado por la Universidad a presentar 2 trabajos</li>
                    <li><br></li>
                    <li>29 de Abril al 15 de Mayo 2011 - Exposición Individual Municipalidad de Ezeiza, Direccion de Cultura.</li>
                    <li><br></li>
                    <li>27 de Abosto 2011 - Juego Provincia de Buenos Aires 2011 <small>(participan artistas plasticos de toda la privincia de Buenos Aires)</small> Partido de La Matanza. Escultura (Primer Premio)</li>
                    <li><br></li>
                    <li>2 al 6 de Octubre 2011 - Final Juegos Provincia de Buenos Aires 2011, Mar del Plata en Escultura (Mencion Especial)</li>
                    <li><br></li>
                    <li>2 al 11 de Marzo 2012 - Exposición individual de Club Saint Thomas, Salon Dorado.</li>
                    <li><br></li>
                    <li>31 de Octubre al 17 de Noviembre 2013 - Exposición Colectiva Teatro Ezeiza "KM ARTE"</li>
                    <li><br></li>
                    <li>4 al 11 de Abril 2014 - Exposición individual de la Universidad de Ezeiza UPE- Primer Muestra con artistas plásticos locales.</li>
                    <li><br></li>
                    <li>14 al 30 de Agosto 2014 Galeria del Abasto Salon Lluvia de Colores Escultura (Primer Premio)</li>
                    <li><br></li>
                    <li>13 al 24 de Abril 2015 - Salon 44° Aniversario de la Galeria Barque, Escultura (Mencion Honorifica)</li>
                    <li><br></li>
                    <li>24 al 30 de Junio 2015 - Exposicion individual Salon "El Telégrafo" Municipalidad de Ezeiza Etcheverria.</li>
                    <li><br></li>
                    <li>19 de Octubre al 2 de Noviembre 2015 - Exposición Colectiva Expo-Arte Visual 2015 Municipalidad de Ezeiza Etcheverria Direccion de Cultura</li>
                    <li><br></li>
                    <li>3 al 11 de Junio 2017 - Exposicion Colectiva Liga Country Sur XVII Muestra de Pintura, Escultura y Fotografia 2017</li>
                </ul>
            </div>
        </div>
    </div>
</div>
@stop
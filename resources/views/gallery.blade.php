@extends('layouts.master')

@section('content')
<!-- Main -->
<div class="wrapper style1">
    <!-- Seccion de galleria ( pasar a otro archivo ) -->
    <div class="wrapper style1">
        <section id="features" class="container special">
            <header>
                <h2>Galeria personal</h2>
                <p>{{ ucfirst($title) }}</p>
            </header>
            <div class="row">
                @if(!$works->count()) <h4>No hay obras cargadas</h4> @endif
                @foreach($works as $work)
                    <article class='col-md-4 col-sm-6 col-xs-12 special'>
                        <a href='{{ route('single.work', $work->id) }}' class='image featured'>
                            <img src='{{ $work->image_url }}' width='300' height='400' alt=''/>
                        </a>
                        <header>
                            <h3><a href='{{ route('single.work', $work->id) }}'>{{ $work->title }}</a></h3>
                        </header>
                        <p>
                            {{ $work->description }}
                        </p>
                    </article>
                @endforeach
            </div>
            @include('layouts.paginate', ['results' => $works])
        </section>
    </div>
</div>
@stop
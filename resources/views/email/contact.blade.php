<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Mensaje de contacto de enriquehofman.com</title>
</head>
<body>
<div>
    <h4>Nombre indicado: {{ $name }}</h4>
</div>
<div>
    <h4>Email del remitente: {{ $email }}</h4>
</div>
<div>
    <h4>Mensaje: {{ $body }}</h4>
</div>

</body>
</html>
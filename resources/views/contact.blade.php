@extends('layouts.master')

@section('content')
    <br>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="well well-sm">
                    <form class="form-horizontal" action="{{ route('send.email') }}" method="post">
                        <fieldset>
                            <legend class="text-center">Formulario de contacto:</legend>

                            <!-- Name input-->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="name">Nombre completo:</label>
                                <div class="col-md-9">
                                    <input id="name" name="name" type="text" placeholder="su nombre" class="form-control">
                                </div>
                            </div>

                            <!-- Email input-->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="email">Su Email:</label>
                                <div class="col-md-9">
                                    <input id="email" name="email" type="email" placeholder="Su email" class="form-control">
                                </div>
                            </div>

                            <!-- Message body -->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="body">Mensaje:</label>
                                <div class="col-md-9">
                                    <textarea class="form-control" id="body" name="body" placeholder="Ingrese su me mensaje aqui..." rows="5"></textarea>
                                </div>
                            </div>

                            {{ csrf_field() }}

                            <!-- Form actions -->
                            <div class="form-group">
                                <div class="col-md-12 text-right">
                                    <button type="submit" class="btn btn-primary btn-lg">Enviar</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
@extends('layouts.master')

@section('content')
<!-- Main -->
<div class="wrapper style1">

    <div class="container">
        <div class="row 200%">
            <div class="4u 12u(mobile)" id="sidebar">
                <hr class="first" />
                <section>
                    <form action="{{ route('single.work.save') }}" method="post">
                        <header>
                            <h3>Deje su opinion de mis trabajos.</h3>
                        </header>
                        <div class="form-group">
                            <label for="name">Nombre: </label>
                            <input type="text" class="form-control" name="name">
                        </div>
                        <div class="form-group">
                            <label for="description">Comentario:</label>
                            <textarea class="form-control" name="description"></textarea>
                        </div>
                        <input type="hidden" name="gallery_id" value="">
                        {{ csrf_field() }}
                        <footer>
                            <button class="btn btn-primary" type="submit">Comentar</button>
                        </footer>
                    </form>
                </section>
                <hr />
                <section>
                    <header>
                        <h3><a href="#">Oras mas comentadas!</a></h3>
                    </header>
                    @if(!$relations->count()) <h4>No hay obras</h4> @endif
                    @foreach($relations as $work)
                        <div class='row 50% no-collapse'>
                            <div class='4u'>
                                <a href='{{ route('single.work', $work->id) }}' class='image fit'>
                                    <img src='{{ url($work->image_url) }}' alt='imagen' />
                                </a>
                            </div>
                            <div class='8u'>
                                <h4>{{ str_limit($work->title, 60) }}</h4>
                                <p>
                                    {{ str_limit($work->description, 60) }}
                                </p>
                            </div>
                        </div>
                    @endforeach
                    <footer>
                        <a href="{{ route('front.gallery') }}" class="button">Ver más</a>
                    </footer>
                </section>
            </div>
            <div class="8u 12u(mobile) important(mobile)" id="content">
                <article id="main">
                    <header>
                        <h2>Comentarios del publico.</h2>
                    </header>
                    <!-- comentarios -->
                    <section>
                        <!-- Contenedor Principal -->
                        @if(!$commentaries->count()) <h4>No existen comentarios generales</h4> @endif
                        <div class="comments-container">
                            <ul id="comments-list" class="comments-list">
                                @foreach($commentaries as $commentary)
                                    <li>
                                        <div class='comment-main-level'>
                                            <!-- Avatar -->
                                            <div class='comment-avatar'><img src='images/perfil.jpg' alt='avatar'></div>
                                            <!-- Contenedor del Comentario -->
                                            <div class='comment-box'>
                                                <div class='comment-head'>
                                                    <h6 class='comment-name'>{{ $commentary->guest_name }}</h6>
                                                    <span>Publicado el {{ $commentary->created_at }}</span>
                                                </div>
                                                <div class='comment-content'>
                                                    {{ $commentary->comment }}
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                            @include('layouts.paginate', ['results' => $commentaries])
                        </div>
                    </section>
                </article>
            </div>
        </div>
    </div>

</div>
@stop
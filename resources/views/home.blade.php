@include('layouts.head')
    <div id="page-wrapper">

    <!-- Header -->
    <div id="header">

        <!-- Inner -->
        <div class="inner">
            <header>
                <hr>
                <a href="{{route('home')}}" style="font-size: 75px;" id="logo">
                    <img src="{{ url('/images/firm.png') }}" style="height: 400px;">
                </a>
                <hr>
            </header>
            <footer>
                <a href="#banner" class="button circled scrolly">Ver Más</a>
            </footer>
        </div>

        <!-- Nav -->
        @include('layouts.menuNav')

    </div>

    <!-- Banner -->
    <section id="banner">
        <header>
            <h2>Enrique hofman</h2>
        </header>
    </section>

    <!-- Carousel -->
    <section class="carousel">
        <header>
            <center>
                <h3>Obras mas recientes.</h3>
            </center>
        </header>
        <div class="reel">
            @foreach($works as $work)
                <article>
                    <a href='{{ route('single.work', $work->id) }}' class='image featured'>
                        <img src='{{ url($work->image_url) }}' style="max-height: 450px;" alt='{{ $work->title }}' />
                    </a>
                    <header>
                        <h3><a href='{{ route('single.work', $work->id) }}'>{{ $work->title}}</a></h3>
                    </header>
                    <p></p>
                </article>
            @endforeach
        </div>
    </section>
</div>
@include('layouts.foot')
@extends('layouts.master')

@section('content')
    <div class="wrapper postClass style1">
        <div class="container">
            <div class="row 200%">
                <div class="8u 12u(mobile)" id="content">
                    <article id="main">
                        <header>
                            <h2>Titulo: {{ $work->title }}</h2>
                            <p>
                                <span>Medidas: </span>{{ $work->measurements }}
                                <br>
                                Descripcion: <br>
                                {{ $work->description }}
                            </p>
                        </header>
                        <a data-toggle="modal" data-target="#myModal" target="_blank" class="image featured">
                            <img src="{{ url($work->image_url) }}" alt="">
                        </a>
                        <section>
                            <header>
                                <h3>Comente que le parecio esta obra:</h3>
                            </header>
                            <form action="{{ route('single.work.save') }}" method="post">
                                <label for="name">Nombre:</label>
                                <input type="text" name="name" required minlength="4" maxlength="60">
                                <input type="hidden" name="gallery_id" value="{{ $work->id  }}">
                                <input type="hidden" name="ip" value="">
                                <label for="comment">Comentario</label>
                                <textarea name="description" id="coment_box" cols="10" rows="3" minlength="3" maxlength="300" required></textarea>
                                {{ csrf_field() }}
                                <input type="submit" value="Comentar">
                            </form>
                        </section>
                        <!-- comentarios -->
                        <section id="comment-section">
                            <!-- Contenedor Principal -->
                            <div class="comments-container">
                                @if(!$work->comments->count())
                                    <h4>Sin comentarios</h4>
                                @else
                                    <ul id="comments-list" class="comments-list">
                                        @foreach($work->comments as $comment)
                                        <li>
                                            <div class='comment-main-level'>
                                                <!-- Avatar -->
                                                <div class='comment-avatar'><img src='{{ url('/images/avatar.jpg') }}' alt='avatar'></div>
                                                <!-- Contenedor del Comentario -->
                                                <div class='comment-box'>
                                                    <div class='comment-head'>
                                                        <h6 class='comment-name'>{{ $comment->guest_name }}</h6>
                                                        <span>{{ $comment->creation_at }}</span>
                                                    </div>
                                                    <div class='comment-content'>
                                                        {{ $comment->comment }}
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </div>
                        </section>
                    </article>
                </div>
                <div class="4u 12u(mobile)" id="sidebar">
                    <hr class="first" />
                    <section>
                        <header>
                            <h3>{{ $work->title }}</h3>
                        </header>
                        <p>
                            {{ $work->description }}
                        </p>
                    </section>
                    <hr />
                    <section>
                        <header>
                            <h3>Obras relacionadas de {{ $work->category->name }}:</h3>
                        </header>
                        <div class='row'>
                            @if(!$relations->count()) <h4>No hay obras relacionadas</h4> @endif
                            @foreach($relations as $relation)
                                <div class="col-sm-12">
                                    <div class='col-sm-4'>
                                        <a href='{{ route('single.work', $relation->id) }}' class='image fit'>
                                            <img src='{{ url($relation->image_url) }}' style="max-height: 120px;" alt='' />
                                        </a>
                                    </div>
                                    <div class='col-sm-8'>
                                        <h4>{{ $relation->title }}</h4>
                                        <p>
                                            {{ $relation->description }}
                                        </p>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="{{ url($work->image_url) }}" style="max-height: 1000px; margin: auto;" class="img-responsive">
                </div>
            </div>
        </div>
    </div>

@stop

@section('js')
    <script type="application/javascript" src="{{ url('js/custom.js') }}"></script>
@stop
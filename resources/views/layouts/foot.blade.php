<!-- Footer -->
<div id="footer">
    <div class="container">
        <div class="row">
            <div class="12u">
                <!-- Copyright -->
                <div class="copyright">
                    <ul class="menu">
                        <li>&copy; Obras creadas por el artista Enrique Hofman.</li><li>Diseñado por Agustin</li>
                    </ul>
                </div>

            </div>

        </div>
    </div>
</div>
</div>
<!-- Scripts -->
<script src="{{ url('js/jquery.min.js') }}"></script>
<script src="{{ url('js/jquery.dropotron.min.js') }}"></script>
<script src="{{ url('js/jquery.scrolly.min.js') }}"></script>
<script src="{{ url('js/jquery.onvisible.min.js') }}"></script>
<script src="{{ url('js/skel.min.js') }}"></script>
<script src="{{ url('js/util.js') }}"></script>
    <!--[if lte IE 8]><script src="{{ url('js/ie/respond.min.js') }}"></script><![endif]-->
<script src="{{ url('js/main.js') }}"></script>
<script src="{{ url('js/jquery.nicescroll.js') }}"></script>
<script src="{{ url('js/bootstrap/bootstrap.min.js') }}"> </script>

@yield('js')

</body>
</html>
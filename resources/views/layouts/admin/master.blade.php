@include('layouts.admin.head')

@if(auth()->user())
    @include('layouts.admin.nav')
@endif
{{--Contenido--}}
@yield('content')

@yield('additional')

@include('layouts.admin.foot')
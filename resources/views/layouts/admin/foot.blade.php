<div class="footer">
    <p>&copy; Design by <a>Agustin</a></p>
</div>
<!--//footer-->
</div>
<!-- Classie -->
<script src="{{url('/js/admin/classie.js')}}"></script>
<script>
    var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
        showLeftPush = document.getElementById( 'showLeftPush' ),
        body = document.body;

    showLeftPush.onclick = function() {
        classie.toggle( this, 'active' );
        classie.toggle( body, 'cbp-spmenu-push-toright' );
        classie.toggle( menuLeft, 'cbp-spmenu-open' );
        disableOther( 'showLeftPush' );
    };

    function disableOther( button ) {
        if( button !== 'showLeftPush' ) {
            classie.toggle( showLeftPush, 'disabled' );
        }
    }
</script>
<!--scrolling js-->
<script src="{{url('/js/jquery.nicescroll.js')}}"></script>
<script src="{{url('js/admin/scripts.js')}}"></script>
<!--//scrolling js-->
<!-- Bootstrap Core JavaScript -->
<script src="{{url('js/bootstrap/bootstrap.min.js')}}"> </script>
</body>
</html>
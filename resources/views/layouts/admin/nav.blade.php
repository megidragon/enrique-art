<!--left-fixed -navigation-->
<div class=" sidebar" role="navigation">
    <div class="navbar-collapse">
        <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
            <ul class="nav" id="side-menu">
                <li>
                    <a href="{{route('admin')}}"><i class="fa fa-home nav_icon"></i>Panel</a>
                </li>
                <li>
                    <a href="{{route('admin.gallery')}}"><i class="fa fa-folder-o nav_icon"></i>Galeria de obras</a>
                </li>
                <li>
                    <a href="{{route('admin.commentary')}}"><i class="fa fa-comments nav_icon"></i>Comentarios de publico</a>
                </li>
                <li>
                    <a href="{{route('logout')}}"><i class="fa fa-user nav_icon"></i>Salir</a>
                </li>
            </ul>
            <div class="clearfix"> </div>
            <!-- //sidebar-collapse -->
        </nav>
    </div>
</div>
<!--left-fixed -navigation-->
<!-- header-starts -->
<div class="sticky-header header-section ">
    <div class="header-left">
        <!--toggle button start-->
        <button id="showLeftPush"><i class="fa fa-bars"></i></button>
        <!--toggle button end-->
        <!--logo -->
        <div class="logo">
            <a href="{{route('home')}}">
                <h1>Enrique Art</h1>
                <span>Administracion</span>
            </a>
        </div>
    </div>
</div>
<!-- //header-ends -->
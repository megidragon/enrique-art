<!DOCTYPE HTML>
<html>
<head>
    <title>Admin - Enrique Arts</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

    <link href="{{url('css/bootstrap/bootstrap.min.css')}}" rel='stylesheet' type='text/css' />
    <link href="{{url('css/admin/style.css')}}" rel='stylesheet' type='text/css' />
    <link href="{{url('css/font-awesome.css')}}" rel="stylesheet">

    <script type="text/javascript" src="{{url('/js/jquery.min.js')}}"></script>
    <script src="{{url('js/admin/modernizr.custom.js')}}"></script>

    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href="{{url('css/admin/animate.css')}}" rel="stylesheet" type="text/css" media="all">

    <script src="{{url('js/admin/wow.min.js')}}"></script>
    <script>
        new WOW().init();
    </script>
    <script src="{{url('js/admin/metisMenu.min.js')}}"></script>
    <script src="{{url('js/admin/custom.js')}}"></script>

    <link href="{{url('css/admin/custom.css')}}" rel="stylesheet">
</head>
<body class="cbp-spmenu-push">
@if(count($errors) > 0)
    <ul>
        @foreach($errors->all() as $error)
            <li class="alert alert-danger">{{$error}}</li>
        @endforeach
    </ul>
@endif
<div class="main-content">
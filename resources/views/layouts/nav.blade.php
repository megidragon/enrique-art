<div id="page-wrapper">

    <!-- Header -->
    <div id="header">

        <!-- Inner -->
        <div class="inner">
            <header>
                <h1><a href="{{url('/home')}}" id="logo">ENRIQUE HOFMAN</a></h1>
            </header>
        </div>

        <!-- Nav -->
       @include('layouts.menuNav')
    </div>
    <br>
    @if (session('status'))
        <div class=" col-md-offset-4 col-md-4 alert @if(session('type')) alert-{{ session('type') }} @else alert-info @endif">
            {{ session('status') }}
        </div>
    @endif
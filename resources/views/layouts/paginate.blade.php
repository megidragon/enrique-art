@if($results->total() > $results->perPage())
    <div class="row">
        @if($results->nextPageUrl())
            <div class="col-xs-1 pull-right">
                <a href='{{ $results->nextPageUrl() }}'>
                    <button class="btn btn-default">Siguiente</button>
                </a>
            </div>
        @endif
        @if($results->previousPageUrl())
            <div class="col-xs-1 pull-right">
                <a href='{{ $results->previousPageUrl() }}'>
                    <button class="btn btn-default">Anterior</button>
                </a>
            </div>
        @endif
    </div>
@endif
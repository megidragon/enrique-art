<nav id="nav">
    <ul>
        <li><a href="{{ route('home') }}">INICIO</a></li>
        <li>
            <a href="{{ route('front.gallery') }}">OBRAS</a>
            <ul>
                <li><a href="{{ route('front.gallery', 'pinturas') }}">Pinturas</a></li>
                <li><a href="{{ route('front.gallery', 'esculturas') }}">Esculturas</a></li>
                <li><a href="{{ route('front.gallery', 'esculto_pinturas') }}">Esculto Pinturas</a></li>
                <li><a href="{{ route('front.gallery', 'vitromosaiquismo') }}">Vitromosaiquismo</a></li>
            </ul>
        </li>
        <li><a href="{{ route('front.resume') }}">CURRICULUM</a></li>
        <li><a href="{{ route('front.commentary') }}">COMENTARIOS</a></li>
        <li><a href="{{ route('front.contact') }}">CONTACTO</a></li>
    </ul>
</nav>
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $fillable = [
        'guest_name',
        'comment',
        'gallery_id'
    ];

    public function gallery()
    {
        return $this->belongsTo('App\Gallery');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Views extends Model
{
    protected $fillable = [
        'gallery_id',
        'ip'
    ];
}

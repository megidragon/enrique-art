<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';

    public function gallery()
    {
        return $this->hasMany('App\Gallery');
    }

}

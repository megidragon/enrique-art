<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GalleryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'category_id' => 'required',
            'image' => 'required|mimes:jpeg,jpg,gif,bmp,png',
            'measurements' => 'required|max:255',
            'creation_date' => 'required|max:255',
            'description' => 'max:255',
            'technique' => 'max:255'
        ];
    }
}

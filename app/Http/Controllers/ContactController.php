<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;
use Illuminate\Support\Facades\Mail;
use Mockery\Exception;

class ContactController extends Controller
{
    public function sendEmailContact(ContactRequest $request)
    {
        try
        {
            $data = $request->all();
            $view = 'email.contact';

            //se envia el array y la vista lo recibe en llaves individuales {{ $email }} , {{ $subject }}...
            Mail::send($view, $data, function ($msg) use ($request) {
                $msg->from(env('FROM_MAIL'));
                //asunto
                $msg->subject('Mensaje de contacto de enriquehofman.com');

                //receptor
                $msg->to(env('CONTACT_MAIL'), env('CONTACT_NAME'));
            });

            return redirect()->back()->with(['status' => 'Se envio el Email. En los proximos dias le respondera..', 'type' => 'success']);
        }catch (Exception $e)
        {
            return redirect()->back()->with(['status' => 'Error: '.$e->getMessage(), 'type' => 'error']);
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Views;
use App\Gallery;
use App\Category;
use App\Comments;

class HomeController extends Controller
{

    /**
     * HomeController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        Views::firstOrCreate(['ip' => $request->ip()]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gallery = new Gallery;
        $recentWorks = $gallery->orderBy('id', 'desc')->take(10)->get();

        return view('home', ['works' => $recentWorks]);
    }

    public function frontGallery($category_key = null)
    {
        $category = new Category;
        $gallery = new Gallery;

        if($category_key)
        {
            $currentCategory = $category->where('name', $category_key)->first();

            if(!$currentCategory->count())
            {
                return abort(404);
            }

            $title = $currentCategory->name;
            $works = $gallery->where('category_id', $currentCategory->id)->paginate(6);
        }
        else
        {
            $works = $gallery->paginate(6);
            $title = 'todas las obras.';
        }

        return view('gallery', ['works' => $works, 'title' => $title]);
    }

    public function singleWork($id)
    {
        $gallery = new Gallery;

        $work = $gallery->with('category', 'comments')->findOrFail($id);
        $relations = $gallery->where([
            'category_id' => $work->category->id
        ])->inRandomOrder()->take(5)->get()->except($work->id);

        return view('work', ['work' => $work, 'relations' => $relations]);
    }

    public function frontCommentary()
    {
        $comments = new Comments;
        $gallery = new Gallery;

        $comentaries = $comments->where('gallery_id', null)->orderBy('created_at', 'desc')->paginate(12);
        $relations = $gallery->inRandomOrder()->take(5)->get();

        return view('commentary', ['commentaries' => $comentaries, 'relations' => $relations]);
    }

    public function frontResume()
    {
        return view('resume');
    }

    public function frontContact()
    {
        return view('contact');
    }
}

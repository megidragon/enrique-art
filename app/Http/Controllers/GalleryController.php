<?php

namespace App\Http\Controllers;

use App\Http\Requests\GalleryRequest;
use App\Http\Requests\GalleryUpdateRequest;
use App\Gallery;
use App\Category;

class GalleryController extends Controller
{
    public function index()
    {
        $category = new Category;
        $gallery = new Gallery;

        $categories = $category->all();
        $galleries = $gallery->orderByDesc('id')->paginate(12);

        return view('admin.gallery.gallery', ['categories' => $categories, 'galleries' => $galleries]);
    }

    public function form()
    {
        $category = new Category;
        $categories = $category->all();

        return view('admin/gallery/form', ['categories' => $categories]);
    }

    public function editForm($id)
    {
        $category = new Category;
        $gallery = new Gallery;

        $work = $gallery->findOrFail($id);
        $categories = $category->all();

        return view('admin.gallery.edit', compact('work', 'categories'));
    }

    public function edit(GalleryUpdateRequest $request, $id)
    {
        $gallery = new Gallery;

        $work = $gallery->findOrFail($id);

        $work->fill($request->toArray());

        $work->save();

        if(isset($request->continue)) {
            return back()->with('status', 'Obra publicada con exito.');
        }else{
            return redirect()->route('admin.gallery')->with('status', 'Obra publicada con exito.');
        }

    }

    public function delete($id)
    {
        $gallery = Gallery::findOrFail($id);
        $gallery->delete();
        return redirect()->route('admin.gallery')->with(['status'=>'Obra eliminada exitosamente.', 'type'=>'success']);
    }

    public function save(GalleryRequest $request)
    {
        $gallery = new Gallery;
        $imagePath = $this->saveImage($request);

        $gallery->fill([
            'title' => $request['title'],
            'user_id' => auth()->id(),
            'category_id' => $request['category_id'],
            'image_url' => $imagePath,
            'measurements' => $request['measurements'],
            'description' => $request['description'],
            'technique' => $request['technique'],
            'creation_date' => $request['creation_date']
        ]);

        $gallery->save();

        if(isset($request->continue)) {
            return back()->with('status', 'Obra publicada con exito.');
        }else{
            return redirect()->route('admin.gallery')->with('status', 'Obra publicada con exito.');
        }
    }

    private function saveImage($request)
    {
        if(!$request->hasFile('image')){
            return '#';
        }
        $file = $request->file('image');
        $photoName = time().'.'.$file->getClientOriginalExtension();
        $path = '/images/gallery/';
        $realPath = public_path().$path;

        $request->image->move($realPath, $photoName);

        return $path.$photoName;
    }
}

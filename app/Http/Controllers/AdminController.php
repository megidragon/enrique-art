<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Views;
use App\Gallery;
use App\Comments;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(){
        $views = new Views;
        $gallery = new Gallery;
        $comments = new Comments;
        $bestWorks = $gallery->whereHas('comments')
            ->withCount('comments')
            ->orderBy('comments_count', 'DESC')
            ->take(5)
            ->get();

        $commentsCount = $comments->where('created_at', '>=', Carbon::today())->get()->count();
        $last5Comments = $comments->orderByDesc('created_at')->take(6)->get();

        $viewsTotal = $views->all()->count();
        $viewsToday = $views->whereDate('created_at', '>=', date('Y-m-d'))->count();

        return view('admin/dashboard', compact('viewsTotal', 'viewsToday', 'bestWorks', 'commentsCount', 'last5Comments'));
    }
}

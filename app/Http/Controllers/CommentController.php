<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comments;
use App\Http\Requests\CommentRequest;

class CommentController extends Controller
{
    public function adminComments($id=null)
    {
        $comments = new Comments;
        if($id)
        {
            $commentaries = $comments->where('gallery_id', $id)->with('gallery')->paginate(12);
        }
        else
        {
            $commentaries = $comments->with('gallery')->paginate(12);
        }

        return view('admin.comments', compact('commentaries'));
    }

    public function save(CommentRequest $request)
    {
        $comments = new Comments;

        $comments->fill([
            'guest_name' => $request->name,
            'comment' => $request->description,
            'gallery_id' => $request->gallery_id
        ]);

        $comments->save();

        return redirect()->back()->with('status', 'Comentario publicado. Muchas gracias por compartir tu opinion con nosotros.');
    }
}

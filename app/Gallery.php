<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table = 'gallery';

    protected $fillable = [
        'title',
        'user_id',
        'category_id',
        'measurements',
        'technique',
        'creation_date',
        'image_url',
        'description',
        'created_at',
        'updated_at'
    ];

    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }

    public function comments()
    {
        return $this->hasMany('App\Comments');
    }
}
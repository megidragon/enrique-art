<?php

Route::get('/', 'HomeController@index')->name('home');
Route::get('/galeria/{category_key?}', 'HomeController@frontGallery')->name('front.gallery');
Route::get('/obra/{id}', 'HomeController@singleWork')->name('single.work');
Route::get('/curriculum', 'HomeController@frontResume')->name('front.resume');
Route::get('/comentarios', 'HomeController@frontCommentary')->name('front.commentary');
Route::get('/contacto', 'HomeController@frontContact')->name('front.contact');
Route::post('/obra/save', 'CommentController@save')->name('single.work.save');
Route::post('/send', 'ContactController@sendEmailContact' )->name('send.email');


Route::get('/admin/login', function(){
    return view('admin/login');
})->name('login');

Route::post('/admin/login', 'Auth\LoginController@login')->name('login.post');
Route::get('/admin/logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function ()
{
    Route::get('/', 'AdminController@index')->name('admin');
    Route::get('/comments/{id?}', 'CommentController@adminComments')->name('admin.commentary');

    //Galeria
    Route::get('/obras', 'GalleryController@index')->name('admin.gallery');
    Route::get('/obra/formulario', 'GalleryController@form')->name('admin.gallery.form');
    Route::get('/obra/editar/{id}', 'GalleryController@editForm')->name('admin.gallery.edit');
    Route::post('/obra/editar/{id}', 'GalleryController@edit')->name('admin.gallery.edit');
    Route::post('/obra/delete/{id}', 'GalleryController@delete')->name('admin.gallery.delete');
    Route::post('/obra/save', 'GalleryController@save')->name('admin.gallery.save');

});